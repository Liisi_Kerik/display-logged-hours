import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

void checkEnvField<T>(String name, T Function(String) fromString) {
  if (!dotenv.env.containsKey(name))
    throw "The environment should have a field named $name.";
  try {
    fromString(dotenv.env[name]);
  } catch (err) {
    throw "Environment field $name does not have the correct type $T. $err";
  }
}

Future<void> checkEnv() async {
  var envFilePath = ".env";

  try {
    await dotenv.load(fileName: envFilePath);
  } catch (err) {
    throw "Failed to load the environment from $envFilePath. $err";
  }

  checkEnvField<String>("TOKEN", (s) => s);
}

Future<T> req<Data, T>(String uri, T Function(Data) fromData) async {
  var token = dotenv.env["TOKEN"];
  var headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
    "Authorization": "Basic " + base64Url.encode(utf8.encode("$token:api_token"))
  };

  var rsp = await http.get(Uri.parse(uri), headers: headers);
  if (rsp.statusCode != 200)
    throw "unexpected response ${rsp.statusCode} ${rsp.body}";

  var body = json.decode(rsp.body);
  if (body["data"] == null)
    throw "no data $body";

  try {
    return fromData(body["data"]);
  } catch (err) {
    throw "Failed to construct an object of type $T from ${body["data"]}. $err";
  }
}

void main() async {
  await checkEnv();
  runApp(DisplayLoggedHours());
}

class DisplayLoggedHours extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        scaffoldBackgroundColor: const Color(0xff42324c)
      ),
      home: DisplayLoggedHoursHomePage()
    );
  }
}

class DisplayLoggedHoursHomePage extends StatefulWidget {
  DisplayLoggedHoursHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class Me {
  String avatarUrl;
  String name;
  int uid;
  int wid;

  Me(Map<String, Object> data) {
    avatarUrl = data["image_url"];
    name = data["fullname"];
    uid = data["id"];
    wid = data["default_wid"];
  }
}

class Task {
  int id;
  int timeInMilliseconds;

  Task(Map<String, Object> task) {
    id = task["id"];
    timeInMilliseconds = task["time"];
  }
}

class _MyHomePageState extends State<DisplayLoggedHoursHomePage> {
  var completer = Completer();
  String _avatarUrl;
  String _name;
  double _total;

  void initState() {
    super.initState();
    _getData();
  }

  Future<void> _getData() async {
    var me = await req<Map<String, Object>, Me>(
      "https://api.track.toggl.com/api/v8/me",
      (data) => Me(data));
    var wid = me.wid;
    var uid = me.uid;

    var startOfWeek = DateTime.now().subtract(Duration(days: DateTime.now().weekday - 1));
    var since = DateFormat("yyyy-MM-dd").format(startOfWeek);
    var thisWeekUri = "https://api.track.toggl.com/reports/api/v2/summary?workspace_id=$wid&user_ids=$uid&since=$since&grouping=users&subgrouping=tasks&user_agent=api_test";

    var summary = await req<List<Object>, List<Task>>(
      thisWeekUri,
      (data) => data.map((task) => Task(task)).toList());

    var totalMilliseconds = summary
      .where((task) => task.id == uid && task.timeInMilliseconds != null)
      .fold(0, (acc, task) => acc + task.timeInMilliseconds);

    if (!completer.isCompleted) {
      completer.complete();
    }
    setState(() {
      _avatarUrl = me.avatarUrl;
      _name = me.name;
      _total = totalMilliseconds / 3600000;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(child: RefreshIndicator(
        child: SingleChildScrollView(
          child: FutureBuilder(
            future: completer.future,
            builder: (context, snapshot) {
              if(snapshot.connectionState != ConnectionState.done)
                return Center(child: CircularProgressIndicator());
              return Column(
                children: [
                  Container(
                    decoration:BoxDecoration(
                      image: DecorationImage(image: NetworkImage(_avatarUrl)),
                      shape: BoxShape.circle
                    ),
                    height: 100,
                    width: 100
                  ),
                  Text(_name, style: TextStyle(
                    color: Color(0xfff8e2e9),
                    fontSize: 20,
                    fontWeight: FontWeight.w900
                  )),
                  Text("${_total.toStringAsFixed(1)}h", style: TextStyle(
                    color: Color(0xfff8e2e9),
                    fontSize: 18,
                    fontWeight: FontWeight.w700
                  ))
                ],
                mainAxisAlignment: MainAxisAlignment.center);
            }
          ),
          physics: AlwaysScrollableScrollPhysics()
        ),
        onRefresh: _getData
      ))
    );
  }
}
